require 'thread'

Thread.abort_on_exception = true

class Naranjo
  CRECIMIENTO = 1
  EDAD_PRODUCTORA = 3
  EDAD_MAXIMA = 20
  NARANJAS_PRODUCIDAS = 50

  attr_reader :altura, :edad, :contador 
  def initialize
    @altura = 0
    @edad = 0
    @contador = 0
  end

  def uno_mas
	@edad += 1
    if (@edad == EDAD_MAXIMA)
      puts "El naranjo ha muerto :'("
    elsif (@edad >= EDAD_PRODUCTORA)
     # @edad += 1
      @altura += CRECIMIENTO
      @contador = (rand(@edad..EDAD_MAXIMA) * Math.log(@edad) * NARANJAS_PRODUCIDAS).to_i  #lolwut
      puts "Edad: #{@edad}; hay #{@contador} naranjas"
    else
      @edad += 1
      @altura += CRECIMIENTO
      print "Edad: #{@edad}; es demasiado joven para producir naranjas\n"
    end
  end

  def recolector
    if (@edad == EDAD_MAXIMA)
      print "El naranjo ha muerto :_(\n"
    elsif (@contador > 0)
      puts "Edad: #{@edad}; hay #{@contador} naranjas"
      @contador -= 1
      print "Rica naranja!\n"
    else
      print "No hay naranjas\n"
    end
  end

end

class Worker
  attr_reader :naranjo
  def initialize(mutex, cv, naranjo)
    @mutex = mutex
    @cv = cv
    @naranjo = naranjo
  end

  def trabajar
    orange_picker = Thread.new do
      until (@naranjo.edad == Naranjo::EDAD_MAXIMA)
        sleep_time = 5 #rand(0..5)
        print "Trabajador durmiendo durante #{sleep_time}\n"
        sleep(sleep_time)
        print "Trabajador despierto despues de #{sleep_time}\n"
        @mutex.synchronize do
          @naranjo.recolector
          print "Trabajador esperando...\n"
          if @naranjo.edad < Naranjo::EDAD_MAXIMA
            @cv.wait(@mutex)
          end
        end
      end
      print "Dejando de trabajar\n"
    end

    age_increaser = Thread.new do
      until (@naranjo.edad == Naranjo::EDAD_MAXIMA)
        sleep_time = 5 #rand(0..5)
        print "El tiempo pasa en #{sleep_time}\n"
        sleep(sleep_time)
        print "El tiempo ha pasado despues de #{sleep_time}\n"
        @mutex.synchronize do
          @naranjo.uno_mas
          print "El naranjo es mas viejo ahora\n"
          @cv.signal
        end
      end
      print "El tiempo ha terminado\n"
    end

    # devolvemos los threads para que se pueda invocar al join en el main
    [orange_picker, age_increaser]
  end

end

worker = Worker.new(Mutex.new, ConditionVariable.new, Naranjo.new)
threads = worker.trabajar

threads.each {|t| t.join}
